<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Trang chủ</title>
</head>
<body>
<div class="container">
  <div id="blog" class="blog">
      <form action="<c:url value='/post/list'/>" method="get" id="formSubmit">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <c:if test="${empty model.listResult}">
                    <div class="row">
                        <div class="alert alert-danger">
                            <p> Không tìm thấy bài viết </p>
                        </div>
                    </div>
                </c:if>
                    <c:forEach var="item" items="${model.listResult}">
                        <div class="row gy-4 posts-list">
                                <div class="col-lg-4">
                                        <div class="img-thumbnail">
                                            <img src="${item.thumbnail}" class="img-fluid" style="width:240px;height:140px">
                                        </div>
                                </div>
                                <div class="col-lg-8">
                                    <article class="d-flex flex-column">
                                        <h2 class="title">
                                            <c:url var="newDetail" value="/blog/blogsingle">
                                                <c:param name="id" value="${item.id}"/>
                                            </c:url>
                                            <a href="${newDetail}">${item.title}</a>
                                        </h2>
                                        <div class="meta-top">
                                            <ul>
                                                <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a href="blog-details.html">${item.createdBy}</a></li>
                                                <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a href="blog-details.html"><fmt:formatDate pattern="dd-MM-yyyy" value="${item.createdDate}"/></a></li>
                                            </ul>
                                        </div>
                                        <div class="content">
                                            <p>${item.shortDescription}</p>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </c:forEach>
            </div>
            <div class="col-lg-4">
                <div class="sidebar">
                    <h3 class="sidebar-title">Search</h3>
                    <div class="sidebar-item search-form input-group" style="background: #fff;border: 1px solid #ddd;padding: 1px 1px; position: relative;">
                        <c:if test="${not empty keyword}">
                            <input type="text" class="form-control" name="keyword" id="keyword" value="${keyword}" placeholder="Nhập từ khoa tìm kiếm">
                        </c:if>
                        <c:if test="${empty keyword}">
                            <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Nhập từ khoa tìm kiếm">
                        </c:if>
                        <div class="input-group-append">
                            <button type="button" id="btnSearch" class="btn btn-outline-secondary" style="background: #5ca3ea;color: #fff;"><i class="bi bi-search"></i></button>
                            <button type="button"class="btn btn-outline-secondary" onclick="clearFilter()" style="background: #5ca3ea; color: #fff;"><i class="bi bi-arrow-clockwise"></i></button>
                        </div>
                    </div>
                    <div class="sidebar-item categories">
                        <h3 class="sidebar-title">Categories</h3>
                        <c:forEach var="category" items="${categoryList}">
                            <c:url var="postListById" value="/post/list/postbycategoryid">
                                <c:param name="id" value="${category.id}"/>
                            </c:url>
                            <ul class="mt-3">
                                <li><a href='${postListById}'>${category.name} </a></li>
                            </ul>
                        </c:forEach>
                    </div>
                        <div class="sidebar-item recent-posts">
                            <h3 class="sidebar-title">Recent Posts</h3>
                            <div class="mt-3">
                                <div class="post-item mt-3">
                                    <c:forEach var="newRecentPost" items="${newRecentPost}">
                                        <img src="${newRecentPost.thumbnail}" alt="" class="flex-shrink-0">
                                        <div>
                                            <c:url var="postDetail" value="/blog/blogsingle">
                                                <c:param name="id" value="${newRecentPost.id}"/>
                                            </c:url>
                                            <h4><a href="${postDetail}">${newRecentPost.title}</a></h4>
                                            <time><fmt:formatDate pattern="dd-MM-yyyy" value="${newRecentPost.createdDate}"/></time>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          <input hidden="text" value="" id="page" name="page"/>
          <input hidden="text" value="" id="limit" name="limit"/>
          <ul class="pagination" id="pagination-post" style="margin-top: 20px"></ul>
      </form>
    </div>
</div>
<script>
  var input = document.getElementById("keyword");
  input.addEventListener("keypress", function(event) {
    if (event.key === "Enter") {
      event.preventDefault();
      document.getElementById("btnSearch").click();
    }
  });
  $('#btnSearch').click(function (e) {
    if (currentPage != page) {
      $('#limit').val(5);
      $('#page').val(currentPage);
      $('#formSubmit').submit();
    }
  });
  var currentPage = ${model.page};
  var totalPage = ${model.totalPage};
  $(function () {
    window.pagObj = $('#pagination-post').twbsPagination ({
      totalPages: totalPage,
      visiblePages: 5,
      startPage: currentPage,
      onPageClick: function (event, page) {
        if (currentPage != page) {
          $('#limit').val(5);
          $('#page').val(page);
          $('#formSubmit').submit();
        }
      }
    });
  });

  function clearFilter(){
    window.location = "/post/list";
  }
</script>
</body>
</html>