<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp" %>
<c:url var="newURL" value="/admin/new/list"/>
<c:url var="createNew" value="/admin/new/create"/>
<c:url var="newAPI" value="/api/new"/>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Danh Sách Bài Viết</title>
</head>

<body>

<div class="main-content">
    <form action="<c:url value='/admin/new/list'/> " id="formSubmit" method="GET">
        <div class="main-content-inner">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Danh Sách Bài
                        viết</a></li>
                </ul>
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <c:if test="${not empty message}">
                            <div class="alert alert-${alert}">
                                    ${message}
                            </div>
                        </c:if>
                        <div class="widget-box table-filter">
                            <div class="table-btn-controls">
                                <div class="pull-right tableTools-container">
                                    <div class="dt-buttons btn-overlap btn-group">
                                        <c:url var="createNewURL" value="/admin/new/create"/>
                                        <a flag="info"
                                           class="dt-button buttons-colvis btn btn-white btn-primary btn-bold"
                                           data-toggle="tooltip" title="Thêm bài viết" href='${createNew}'>
															<span>
																<i class="fa fa-plus-circle bigger-110 purple"></i>
															</span>
                                        </a>
                                        <button id="btnDelete" type="button" onclick="warningBeforeDelete()"
                                                class="dt-button buttons-html5 btn btn-white btn-primary btn-bold"
                                                data-toggle="tooltip" title='Xóa bài viết'>
                                                <span>
                                                    <i class="fa fa-trash-o bigger-110 pink"></i>
                                                </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th><input type="checkbox" id="checkAll"></th>
                                            <th>Id</th>
                                            <th>Tên bài viết</th>
                                            <th>Mô tả ngắn</th>
                                            <th>Ảnh đại diện</th>
                                            <th>Ngày Đăng</th>
                                            <th>Người Đăng</th>
                                            <th>Thao tác</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:forEach var="item" items="${model.listResult}">
                                            <tr>
                                                <td><input type="checkbox" name="prog" class="checkitem" id="checkAll_${item.id}" value="${item.id}"></td>
                                                <td>${item.id}</td>
                                                <td>${item.title}</td>
                                                <td>${item.shortDescription}</td>
                                                <td><img src="${item.thumbnail}" class="img-rounded"  width="100px" height="80px"> </td>
                                                <td><fmt:formatDate pattern="dd-MM-yyyy" value="${item.createdDate}"/> </td>
                                                <td>${item.createdBy}</td>
                                                <td>
                                                    <c:url var="updateNewURL" value="/admin/new/update">
                                                        <c:param name="id" value="${item.id}"/>
                                                    </c:url>

                                                    <a class="btn btn-sm btn-primary btn-edit"
                                                       data-toggle="tooltip"
                                                       title="Cập nhật bài viết"
                                                       href='${updateNewURL}'><i
                                                            class="fa fa-pencil-square-o"
                                                            aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>

                                    <ul class="pagination" id="pagination"></ul>
                                    <input type="hidden" value="" id="page" name="page"/>
                                    <input type="hidden" value="" id="limit" name="limit"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>

  $(document).ready(function(){
    var submitBtn=$('#btnDelete');
    submitBtn.hide();
    $('input[name="prog"]').change(function(){
      if ($('input[name="prog"]:checked').length > 0) {
        submitBtn.show();
      }
      else {
        submitBtn.hide();
      }
    });
    $("#checkAll").click(function(){
      if(this.checked){
        $('.checkitem').each(function(){
          $(".checkitem").prop('checked', true);
          submitBtn.show();
        })
      }else{
        $('.checkitem').each(function(){
          $(".checkitem").prop('checked', false);
          submitBtn.hide();
        })
      }
    });
  });

  function warningBeforeDelete() {
    swal({
      title: "Are you sure?",
      text: "Are you sure you want to delete this record ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-success",
      cancelButtonClass: "btn-danger",
      confirmButtonText: "Yes, DELETE it!",
      cancelButtonText: "No, cancel please!",
      closeOnConfirm: false,
      closeOnCancel: false
    }).then(function(isConfirm) {
      if (isConfirm.value) {
        var ids = $('tbody input[type=checkbox]:checked').map(function () {
          return $(this).val();
        }).get();
        deleteNew(ids);
      }else {
        swal("Cancelled", "AMC Record is safe :)", "error");
      }
    });
  }
  function deleteNew(data) {
    $.ajax({
      url: '${newAPI}',
      type: 'DELETE',
      contentType: 'application/json',
      data: JSON.stringify(data),
      success: function (result) {
        window.location.href = "${newURL}?message=delete_success";
      },
      error: function (error) {
        window.location.href = "${newURL}?message=error_system";
      }
    });
  }

  // phân trang
  var currentPage = ${model.page};
  var totalPage = ${model.totalPage};
  $(function () {
    window.pagObj = $('#pagination').twbsPagination({
      totalPages: totalPage,
      visiblePages: 10,
      startPage: currentPage,
      onPageClick: function (event, page) {
        if (currentPage != page) {
          $('#limit').val(10);
          $('#page').val(page);
          $('#formSubmit').submit();
        }
      }
    });
  });
</script>
</body>
</html>