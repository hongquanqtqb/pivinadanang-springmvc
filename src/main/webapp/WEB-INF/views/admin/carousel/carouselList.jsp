<%@include file="/common/taglib.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Slide</title>
</head>
<body>
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Slide List</a></li>
                </ul>
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <c:if test="${not empty message}">
                            <div class="alert alert-${alert}">
                                    ${message}
                            </div>
                        </c:if>
                      <div class="widget-box table-filter">
                            <div class="table-btn-controls">
                                <div class="pull-right tableTools-container">
                                    <div class="dt-buttons btn-overlap btn-group">
                                        <c:url var="createCarousel" value="/admin/carousel/create"/>
                                        <a flag="info"
                                           class="dt-button buttons-colvis btn btn-white btn-primary btn-bold"
                                           data-toggle="tooltip" title="Thêm thể loại" href="${createCarousel}">
                                            <span><i class="fa fa-plus-circle bigger-110 purple"></i></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                    </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead class="bg-light">
                                            <tr>
                                                <th>Id</th>
                                                <th>Carousel Url</th>
                                                <th>Người Tạo</th>
                                                <th>Ngày tao</th>
                                                <th>Người sửa</th>
                                                <th>Ngày sửa</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="item" items="${carousel.listResult}">
                                                <tr>
                                                    <td><p class="fw-normal mb-1">${item.id}</p></td>
                                                    <td><img src="${item.carouselurl}" class="img-rounded" style="width: 220px; height: auto"/></td>
                                                    <td><p>${item.createdBy}</p></td>
                                                    <td><p class="fw-normal mb-1">${item.createdDate}</p></td>
                                                    <td><p>${item.modifiedBy}</p></td>
                                                    <td><p>${item.modifiedDate}</p></td>
                                                    <td> <a href="<c:url value="/admin/carousel/delete/${item.id}"/>"
                                                            class="dt-button buttons-html5 btn btn-white btn-danger btn-bold"
                                                            title="Xóa Silde"
                                                            id="deleteButton">
                                                        <span><i class="fa fa-trash-o bigger-110 pink"></i></span>
                                                    </a>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                </table>
                                 <div class="modal" tabindex="-1" role="dialog" id="deletModal">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Confirm delete</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure you want to delete this record ?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="" type="button" class="btn btn-danger" id="delRef">Yes, Delete</a>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
  $(document).ready(function () {
    $('table #deleteButton').on('click',function (event){
      event.preventDefault();
      var href = $(this).attr('href');
      $('#deletModal #delRef').attr('href',href);
      $('#deletModal').modal();
    });
  });

</script>
</body>
</html>