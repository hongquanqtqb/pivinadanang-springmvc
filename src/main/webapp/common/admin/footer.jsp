<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div class="footer">
	<div class="footer-inner">
		<div class="footer-content">
						<span class="bigger-120">
							Designed by <a>Phạm Hồng Quân</a>
							 &copy; Copyright <strong><span>PI VINA DANANG</span></strong>. All Rights Reserved
						</span>
		</div>
	</div>
</div>