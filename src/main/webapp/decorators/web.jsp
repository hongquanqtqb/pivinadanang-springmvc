<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>PI VINA DANANG</title>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <!-- Vendor CSS Files -->
    <link href="<c:url value='/template/web/assets/vendor/bootstrap/css/bootstrap.min.css'/>" rel="stylesheet"/>
    <link href="<c:url value='/template/web/assets/vendor/bootstrap-icons/bootstrap-icons.css'/>" rel="stylesheet"/>
    <link href="<c:url value='/template/web/assets/vendor/bootstrap/js/bootstrap.min.js'/>" rel="stylesheet"/>
    <script type='text/javascript' src='<c:url value="/template/admin/assets/js/jquery-2.2.3.min.js" />'></script>
    <link href="<c:url value='/template/web/assets/css/style.css'/>" rel="stylesheet"/>
</head>
<body>
        <%@include file="/common/web/header.jsp" %>

           <dec:body/>

        <%@include file="/common/web/footer.jsp" %>

        <script src="<c:url value='/template/web/assets/vendor/bootstrap/js/bootstrap.bundle.min.js'/>"></script>
        <script src="<c:url value='/template/web/assets/js/main.js'/>"></script>
        <script src="<c:url value='/template/web/assets/js/comment.js'/>"></script>
        <script src="<c:url value='/template/admin/paging/jquery.twbsPagination.js'/>"></script>
        <script type='text/javascript' src='<c:url value="/template/web/assets/js/moment.min.js" />'></script>
</body>
</html>
