package com.pivinadanang.repository;

import com.pivinadanang.entity.UserEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
  @Query("select u from UserEntity u where u.userName = ?1 and u.status = ?2")
  UserEntity findOneByUserNameAndStatus(String username, int status);

  @Query(value = "select u from UserEntity u where u.id = ?1")
  UserEntity findById (Long id);

  @Query(value = "select  u from UserEntity u where u.userName = ?1")
  List<UserEntity> findByUserName(String userName);

  @Query("select u from UserEntity u order by u.fullName desc ")
  Page<UserEntity> getAllCategoryPaginated(Pageable pageable);
}

