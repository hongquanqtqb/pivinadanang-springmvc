package com.pivinadanang.repository;

import com.pivinadanang.entity.CarouselEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface CarouselRepository extends JpaRepository<CarouselEntity, Long> {
  @Query(value = "select c from CarouselEntity c ")
  List<CarouselEntity> getAll();

  @Modifying
  @Query(value = "delete from CarouselEntity c where c.id = ?1")
  void deleteCarouse(Long id);

  @Query("select c from CarouselEntity c where c.id = ?1")
  CarouselEntity findById(Long id);
}
