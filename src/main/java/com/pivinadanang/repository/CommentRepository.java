package com.pivinadanang.repository;

import com.pivinadanang.dto.CommentDTO;
import com.pivinadanang.entity.CommentEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CommentRepository extends JpaRepository<CommentEntity, Long>{

  @Query(value = "select count(c) from CommentEntity c where c.post.id = ?1")
  int getCountCommentByPostId(Long post_id);

  @Modifying
  @Query(value = "insert into comments (content, post_id) values (:content, :post_id)", nativeQuery = true)
  CommentEntity insertComment (@Param("content") String content, @Param("post_id") Long post_id);

  @Query(value = "select c from CommentEntity c where c.post.id = ?1 order by c.createdDate desc")
  List<CommentEntity> getAllCommentByPostId(Long post_id);

}
