package com.pivinadanang.repository;

import com.pivinadanang.entity.CommentEntity;
import com.pivinadanang.entity.NewEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface NewRepository extends JpaRepository<NewEntity,Long> {

  @Query(value = "select n from NewEntity n where n.id = ?1")
  NewEntity findByID (Long id);

  @Modifying
  @Query(value = "delete from NewEntity n where n.id = ?1")
  void deleteById(Long id);

  @Query("select n from NewEntity n where n.category.id = ?1")
  List<NewEntity> findByCategoryId(Long category_id);

  @Query("select n from NewEntity n order by n.createdDate desc")
  Page<NewEntity> findAll(Pageable pageable);

  Page<NewEntity> findByTitleContaining(String title, Pageable pageable);

  @Query(value = "select n from NewEntity n where n.category.id = ?1 order by n.createdDate desc")
  Page<NewEntity> findAllByCategoryId(Long category_id, Pageable pageable);

  @Query(value = "select count(n.id) from NewEntity n where n.category.id = ?1")
  int countByCategory_Id(Long category_id);

}

