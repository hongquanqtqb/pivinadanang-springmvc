package com.pivinadanang.repository;

import com.pivinadanang.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface RoleRepository extends JpaRepository<RoleEntity, Long> {
  @Query("SELECT r FROM RoleEntity r WHERE r.code = ?1")
   RoleEntity findByCode (String code);
}
