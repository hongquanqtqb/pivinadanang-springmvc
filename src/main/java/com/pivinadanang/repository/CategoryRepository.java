package com.pivinadanang.repository;

import com.pivinadanang.dto.CategoryDTO;
import com.pivinadanang.entity.CategoryEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {

  @Query(value = "select c from CategoryEntity c where c.code = ?1")
  CategoryEntity findOneByCode(String code);

  @Query(value = "select c from CategoryEntity c where c.id = ?1")
  CategoryEntity findById(Long id);


  @Modifying
  @Query(value = "insert into category(code,name) values (:code, :name)", nativeQuery = true)
  void insertCategory (@Param("code") String code, @Param("name") String name);

  CategoryDTO save(CategoryDTO categoryDTO);

  @Query(value = "select c from CategoryEntity c where c.code = ?1")
  List<CategoryEntity> findByCode(String code);

  @Modifying
  @Query(value = "delete from CategoryEntity c where c.id = ?1")
  void deleteById(Long Id);

  @Query(value = "select c from CategoryEntity c")
  List<CategoryEntity> getAllCategory();

  @Query(value = "select c from CategoryEntity c order by c.createdDate desc")
  Page<CategoryEntity> getAllCategoryPaginated(Pageable pageable);

}
