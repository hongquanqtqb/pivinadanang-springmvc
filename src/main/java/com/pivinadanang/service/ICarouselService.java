package com.pivinadanang.service;

import com.pivinadanang.dto.CarouselDTO;
import java.util.List;

public interface ICarouselService {
  List<CarouselDTO> getAll();
  void deleteById(Long id);
  CarouselDTO save (CarouselDTO carouselDTO);
  CarouselDTO findById(Long id);
}
