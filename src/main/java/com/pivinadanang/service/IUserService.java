package com.pivinadanang.service;

import com.pivinadanang.dto.UserDTO;
import com.pivinadanang.entity.UserEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IUserService {
  UserDTO save (UserDTO userDTO);
  UserDTO findById(Long id);
  List<UserEntity> findByUserName(String userName);
  Page<UserEntity> getAllUserPaginated (Pageable pageable);
}
