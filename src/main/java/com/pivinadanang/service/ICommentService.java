package com.pivinadanang.service;

import com.pivinadanang.dto.CommentDTO;
import java.util.List;

public interface ICommentService {
  int getCountCommentByPostId(Long post_id);
  CommentDTO save(CommentDTO commentDTO);

  List<CommentDTO> getAllCommentByPostId(Long post_id);
}
