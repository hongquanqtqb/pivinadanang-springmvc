package com.pivinadanang.service;

import com.pivinadanang.dto.NewDTO;
import com.pivinadanang.entity.NewEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface INewService {
	List<NewDTO> findAll(Pageable pageable);
	int getTotalItem();
	NewDTO findById(Long id);
	NewDTO save(NewDTO dto);
	void delete(long [] ids);
	List<NewDTO> findByCategoryId(Long category_id);

	List<NewDTO> getPostAll(Pageable pageable);
	List<NewDTO> findByTitleContaining(String keyword,Pageable pageable);

	Page<NewEntity> findByCategoryId(Long category_id, Pageable pageable);

	int countByCategory_Id(Long category_id);
}
