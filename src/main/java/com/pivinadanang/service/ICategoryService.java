package com.pivinadanang.service;

import com.pivinadanang.dto.CategoryDTO;
import com.pivinadanang.entity.CategoryEntity;

import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ICategoryService {
	Map<String,String> findAll();
	List<CategoryDTO> getAllCategory();
	List<CategoryEntity> findByCode(String categoryCode);
	CategoryDTO insertOrUpdateCategory (CategoryDTO categoryDTO);
	void delete(Long id);
	Page<CategoryEntity> getAllCategoryPaginated(Pageable pageable);
}
