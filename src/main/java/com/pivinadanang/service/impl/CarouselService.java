package com.pivinadanang.service.impl;

import com.pivinadanang.dto.CarouselDTO;
import com.pivinadanang.entity.CarouselEntity;
import com.pivinadanang.repository.CarouselRepository;
import com.pivinadanang.service.ICarouselService;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class CarouselService implements ICarouselService {

  @Autowired
  private CarouselRepository carouselRepository;

  @Autowired
  private ModelMapper modelMapper;

  @Transactional
  @Override
  public List<CarouselDTO> getAll() {
    List<CarouselDTO> carouselDTOList = carouselRepository.getAll().stream()
        .map(CarouselEntity -> modelMapper.map(CarouselEntity, CarouselDTO.class))
        .collect(Collectors.toList());;
    return carouselDTOList;
  }

  @Transactional
  @Override
  public void deleteById(Long id) {
    carouselRepository.deleteCarouse(id);
  }

  @Transactional
  @Override
  public CarouselDTO save(CarouselDTO carouselDTO) {
    CarouselEntity carouselEntity = carouselRepository.save(modelMapper.map(carouselDTO, CarouselEntity.class));
    CarouselDTO carousel = modelMapper.map(carouselEntity, CarouselDTO.class);
    return carousel;
  }

  @Override
  public CarouselDTO findById(Long id) {
    return modelMapper.map(carouselRepository.findById(id),CarouselDTO.class);
  }
}
