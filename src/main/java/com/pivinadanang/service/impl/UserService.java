package com.pivinadanang.service.impl;

import com.pivinadanang.constant.SystemConstant;
import com.pivinadanang.dto.UserDTO;
import com.pivinadanang.entity.RoleEntity;
import com.pivinadanang.entity.UserEntity;
import com.pivinadanang.repository.RoleRepository;
import com.pivinadanang.repository.UserRepository;
import com.pivinadanang.service.IUserService;
import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService implements IUserService {
  @Autowired
  private UserRepository userRepository;

  @Autowired
  private ModelMapper modelMapper;

  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Override
  @Transactional
  public UserDTO save(UserDTO userDTO) {
    String encodedPassword = passwordEncoder.encode(userDTO.getPassword());
    userDTO.setPassword(encodedPassword);
    userDTO.setStatus(SystemConstant.ACTIVE_STATUS);
    UserEntity userEntity = modelMapper.map(userDTO, UserEntity.class);

    String code = SystemConstant.INCTIVE_ROLE_USER;
    RoleEntity roleEntity = roleRepository.findByCode(code);

    List<RoleEntity> roleEntityList = new ArrayList<>();
    roleEntityList.add(roleEntity);
    userEntity.setRoles(roleEntityList);
    UserEntity userEntityRequest = userRepository.save(userEntity);

    return modelMapper.map(userEntityRequest, UserDTO.class);
  }

  @Override
  public UserDTO findById(Long id) {
    UserEntity userEntity = userRepository.findById(id);
    return modelMapper.map(userEntity, UserDTO.class);
  }

  @Override
  public List<UserEntity> findByUserName(String userName) {
    List<UserEntity> userEntityList = userRepository.findByUserName(userName);
    return userEntityList;
  }

  @Override
  public Page<UserEntity> getAllUserPaginated(Pageable pageable) {
    return userRepository.getAllCategoryPaginated(pageable);
  }
}
