package com.pivinadanang.service.impl;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.pivinadanang.converter.NewConverter;
import com.pivinadanang.dto.NewDTO;
import com.pivinadanang.entity.CategoryEntity;
import com.pivinadanang.entity.NewEntity;
import com.pivinadanang.repository.CategoryRepository;
import com.pivinadanang.repository.NewRepository;
import com.pivinadanang.service.INewService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NewService implements INewService{
	@Autowired
	private NewRepository newRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private NewConverter newConverter;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private Cloudinary cloudinary;

	@Override
	public List<NewDTO> findAll(Pageable pageable) {
		List<NewDTO> newDTOList = newRepository.findAll(pageable).getContent().stream()
				.map(NewEntity -> modelMapper.map(NewEntity, NewDTO.class))
				.collect(Collectors.toList());;
		return newDTOList;
	}

	@Override
	public int getTotalItem() {
		return (int) newRepository.count();
	}



	@Override
	public NewDTO findById(Long id) {
		NewEntity newEntityById =  newRepository.findByID(id);
		return modelMapper.map(newEntityById, NewDTO.class);
	}


	@Override
	@Transactional
	public NewDTO save(NewDTO dto) {
		CategoryEntity category = categoryRepository.findOneByCode(dto.getCategoryCode());
		NewEntity newEntity = new NewEntity();
		if(dto.getId() != null){
			NewEntity oldNew = newRepository.findOne(dto.getId());

			oldNew.setCategory(category);

			newEntity = newConverter.toEntity(oldNew,dto);

		}else {
			newEntity = newConverter.toEntity(dto);
			newEntity.setCategory(category);
		}
		return newConverter.toDto(newRepository.save(newEntity));
	}

	@Override
	@Transactional
	public void delete(long[] ids) {
		for(long id: ids){
			newRepository.deleteById(id);
		}
	}

	@Override
	public List<NewDTO> findByCategoryId(Long category_id) {
		List<NewDTO> newDTOList = new ArrayList<>();
		List<NewEntity> newEntityList = newRepository.findByCategoryId(category_id);
		for(NewEntity item: newEntityList){
			NewDTO newDTO = newConverter.toDtoList(item);
			newDTOList.add(newDTO);
		}
		return newDTOList;
	}
	@Override
	public List<NewDTO> getPostAll(Pageable pageable) {
		List<NewDTO> newDTOList = new ArrayList<>();
		Page<NewEntity> pagePostList = newRepository.findAll(pageable);
		List<NewEntity> newEntityList = pagePostList.getContent();
		for(NewEntity item: newEntityList){
			NewDTO newDTO = newConverter.toDtoList(item);
			newDTOList.add(newDTO);
		}
		return newDTOList;
	}

	@Override
	public List<NewDTO> findByTitleContaining(String keyword, Pageable pageable) {
		 Page<NewEntity> pagePostList = newRepository.findByTitleContaining(keyword, pageable);
		List<NewEntity> newEntityList = pagePostList.getContent();
		List<NewDTO> newDTOList = new ArrayList<>();
		for(NewEntity item: newEntityList){
			NewDTO newDTO = newConverter.toDtoList(item);
			newDTOList.add(newDTO);
		}
		return newDTOList;
	}

	@Override
	public 	Page<NewEntity> findByCategoryId(Long category_id, Pageable pageable) {
		Page<NewEntity> newEntityList = newRepository.findAllByCategoryId(category_id, pageable);
				return newEntityList;
	}

	@Override
	public int countByCategory_Id(Long category_id) {
		return newRepository.countByCategory_Id(category_id);
	}
}






