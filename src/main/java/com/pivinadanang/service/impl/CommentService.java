package com.pivinadanang.service.impl;

import com.pivinadanang.dto.CommentDTO;
import com.pivinadanang.dto.NewDTO;
import com.pivinadanang.entity.CommentEntity;
import com.pivinadanang.entity.NewEntity;
import com.pivinadanang.repository.CommentRepository;
import com.pivinadanang.repository.NewRepository;
import com.pivinadanang.service.ICommentService;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CommentService implements ICommentService {
  @Autowired
  private CommentRepository commentRepository;

  @Autowired
  private NewRepository newRepository;

  @Autowired
  private ModelMapper modelMapper;

  @Override
  public int getCountCommentByPostId(Long post_id) {
    return (int)commentRepository.getCountCommentByPostId(post_id);
  }

  @Transactional
  @Override
  public CommentDTO save(CommentDTO commentDTO) {
    NewEntity newEntity = newRepository.findByID(commentDTO.getPostId());
    CommentEntity commentEntity = modelMapper.map(commentDTO, CommentEntity.class);
    commentEntity.setPost(newEntity);
    return modelMapper.map(commentRepository.save(commentEntity), CommentDTO.class);
  }

  @Override
  public List<CommentDTO> getAllCommentByPostId(Long post_id) {
    List<CommentDTO> commentDTOList = commentRepository.getAllCommentByPostId(post_id)
        .stream()
        .map(CommentEntity -> modelMapper.map(CommentEntity, CommentDTO.class))
        .collect(Collectors.toList());;
    return commentDTOList;
  }
}
