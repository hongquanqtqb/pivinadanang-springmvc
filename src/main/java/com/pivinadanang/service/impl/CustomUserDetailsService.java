package com.pivinadanang.service.impl;

import com.pivinadanang.constant.SystemConstant;
import com.pivinadanang.dto.MyUser;
import com.pivinadanang.entity.RoleEntity;
import com.pivinadanang.entity.UserEntity;
import com.pivinadanang.repository.UserRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {
  @Autowired
  private UserRepository userRepository;
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    // check username va password
    UserEntity userEntity = userRepository.findOneByUserNameAndStatus(username, SystemConstant.ACTIVE_STATUS);
    if(userEntity == null){
      throw new UsernameNotFoundException("user not found");
    }
    // put thông tin vào security để duy trì thông tin khi user login vào hệ thống
    List<GrantedAuthority> authorities = new ArrayList<>();
    for (RoleEntity role: userEntity.getRoles()){
      authorities.add(new SimpleGrantedAuthority(role.getCode()));
    }
    MyUser myUser = new MyUser(userEntity.getUserName(), userEntity.getPassword(), true, true,
                      true, true, authorities);
    myUser.setFullName(userEntity.getFullName());
    return myUser;
  }
}
