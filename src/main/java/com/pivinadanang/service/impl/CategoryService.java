package com.pivinadanang.service.impl;

import com.pivinadanang.converter.CategoryConverter;
import com.pivinadanang.dto.CategoryDTO;
import com.pivinadanang.entity.CategoryEntity;
import com.pivinadanang.repository.CategoryRepository;
import com.pivinadanang.service.ICategoryService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CategoryService implements ICategoryService {
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private CategoryConverter categoryConverter;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public Map<String, String> findAll() {
		Map<String, String> result = new HashMap<>();
		List<CategoryEntity> entity = categoryRepository.findAll();
		for (CategoryEntity item : entity){
			result.put(item.getCode(), item.getName());
		}
		return result;
	}

	@Override
	public List<CategoryDTO> getAllCategory() {
		List<CategoryDTO> categoryDTOList = categoryRepository.getAllCategory().stream()
				.map(CategoryEntity -> modelMapper.map(CategoryEntity, CategoryDTO.class))
				.collect(Collectors.toList());;
		return categoryDTOList;
	}

	@Override
	public List<CategoryEntity> findByCode(String categoryCode) {
		return categoryRepository.findByCode(categoryCode);
	}


	@Override
	@Transactional
	public CategoryDTO insertOrUpdateCategory(CategoryDTO categoryDTO) {
		CategoryEntity categoryEntity = new CategoryEntity();
		if(categoryDTO.getId() != null){
			CategoryEntity oldCategoryEntity = categoryRepository.findById(categoryDTO.getId());
			categoryEntity = categoryConverter.toEntity(oldCategoryEntity,categoryDTO);
		}else {
			categoryEntity = modelMapper.map(categoryDTO, CategoryEntity.class);
		}
		return categoryConverter.toDto(categoryRepository.save(categoryEntity));
	}

	@Override
	@Transactional
	public void delete(Long id) {
		categoryRepository.deleteById(id);
	}

	@Override
	public Page<CategoryEntity> getAllCategoryPaginated(Pageable pageable) {
		return categoryRepository.getAllCategoryPaginated(pageable);
	}

}
