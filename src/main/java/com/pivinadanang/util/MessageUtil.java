package com.pivinadanang.util;

import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component
public class MessageUtil {
 public Map<String, String> getMessage (String message){
    Map<String, String> result = new HashMap<>();
    if(message.equals("update_success")){
      result.put("message","Update success");
      result.put("alert","success");
    }else if(message.equals("insert_success")){
     result.put("message","Insert success");
     result.put("alert","success");
    }else if(message.equals("error_system")){
      result.put("message","Error system");
      result.put("alert","danger");
    }else if(message.equals("delete_success")){
      result.put("message","Deleted success");
      result.put("alert","success");
    }else if(message.equals("already")){
      result.put("message","Already Taken");
      result.put("alert","danger");
    }else if(message.equals("file_missing")){
      result.put("message","image is missing");
      result.put("alert","warning");
    } else if(message.equals("foreign_key")){
      result.put("message","Cannot delete or update a parent row: a foreign key constraint fails !");
      result.put("alert","danger");
    }

    return result;
  }
}
