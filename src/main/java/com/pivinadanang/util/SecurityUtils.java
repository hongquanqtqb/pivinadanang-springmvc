package com.pivinadanang.util;

import com.pivinadanang.dto.MyUser;
import java.util.ArrayList;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {
  // Hàm gét thông tin ra view (username, password, full name...)
  public static MyUser getPrincipal() {
    MyUser myUser = (MyUser) (SecurityContextHolder.getContext()).getAuthentication().getPrincipal();
    return myUser;
  }
  public static List<String> getAuthorities(){
    @SuppressWarnings("unchecked")
      List<String> results = new ArrayList<>();
      List<GrantedAuthority> authorities = (List<GrantedAuthority>)(SecurityContextHolder.getContext().getAuthentication().getAuthorities());
      for (GrantedAuthority authority : authorities) {
        results.add(authority.getAuthority());
      }
      return results;
    }

}
