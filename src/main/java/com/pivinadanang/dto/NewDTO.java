package com.pivinadanang.dto;


import com.pivinadanang.entity.CommentEntity;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

public class NewDTO extends AbstractDTO<NewDTO>{
  @NotEmpty
  private String title;

  private String thumbnail;

  @NotEmpty
  private String shortDescription;

  @NotEmpty
  private String content;


  private Long categoryId;

  @NotEmpty
  private String categoryCode;

  private String publicId;

  private MultipartFile file;

  private List<CommentDTO> comments = new ArrayList<>();


  public NewDTO() {
  }

  public NewDTO(String title, String thumbnail, String shortDescription, String content,
      Long categoryId, String categoryCode,String publicId, List<CommentDTO> comments) {
    this.title = title;
    this.thumbnail = thumbnail;
    this.shortDescription = shortDescription;
    this.content = content;
    this.categoryId = categoryId;
    this.categoryCode = categoryCode;
    this.comments = comments;
    this.publicId = publicId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getThumbnail() {
    return thumbnail;
  }

  public void setThumbnail(String thumbnail) {
    this.thumbnail = thumbnail;
  }

  public String getShortDescription() {
    return shortDescription;
  }

  public void setShortDescription(String shortDescription) {
    this.shortDescription = shortDescription;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Long getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Long categoryId) {
    this.categoryId = categoryId;
  }

  public String getCategoryCode() {
    return categoryCode;
  }

  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }

  public MultipartFile getFile() {
    return file;
  }

  public String getPublicId() {
    return publicId;
  }

  public void setPublicId(String publicId) {
    this.publicId = publicId;
  }

  public void setFile(MultipartFile file) {
    this.file = file;
  }

  public List<CommentDTO> getComments() {
    return comments;
  }

  public void setComments(List<CommentDTO> comments) {
    this.comments = comments;
  }
}
