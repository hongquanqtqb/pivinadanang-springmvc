package com.pivinadanang.dto;

import com.pivinadanang.entity.UserEntity;
import java.util.ArrayList;
import java.util.List;


public class RoleDTO extends AbstractDTO<UserDTO> {

  private String name;

  private String code;

  private List<UserEntity> users = new ArrayList<>();

  public RoleDTO() {
  }

  public RoleDTO(String name, String code, List<UserEntity> users) {
    this.name = name;
    this.code = code;
    this.users = users;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public List<UserEntity> getUsers() {
    return users;
  }

  public void setUsers(List<UserEntity> users) {
    this.users = users;
  }
}
