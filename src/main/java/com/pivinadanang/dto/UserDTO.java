package com.pivinadanang.dto;

import com.pivinadanang.entity.RoleEntity;
import java.util.ArrayList;
import java.util.List;
public class UserDTO extends AbstractDTO<UserDTO>{
  private String userName;
  private String password;
  private String fullName;
  private Integer status;
  private List<RoleEntity> roles = new ArrayList<>();


  public UserDTO() {
  }

  public UserDTO(String userName, String password, String fullName, Integer status, List<RoleEntity> roles) {
    this.userName = userName;
    this.password = password;
    this.fullName = fullName;
    this.status = status;
    this.roles = roles;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public List<RoleEntity> getRoles() {
    return roles;
  }

  public void setRoles(List<RoleEntity> roles) {
    this.roles = roles;
  }
}
