package com.pivinadanang.dto;

import org.springframework.web.multipart.MultipartFile;

public class CarouselDTO extends AbstractDTO<CarouselDTO>{
  private String carouselurl;
  private String publicId;
  private MultipartFile file;

  public CarouselDTO() {
  }

  public CarouselDTO(String carouselurl, MultipartFile file, String publicId) {
    this.carouselurl = carouselurl;
    this.file = file;
    this.publicId = publicId;
  }

  public String getCarouselurl() {
    return carouselurl;
  }

  public void setCarouselurl(String carouselurl) {
    this.carouselurl = carouselurl;
  }

  public MultipartFile getFile() {
    return file;
  }

  public void setFile(MultipartFile file) {
    this.file = file;
  }

  public String getPublicId() {
    return publicId;
  }

  public void setPublicId(String publicId) {
    this.publicId = publicId;
  }
}
