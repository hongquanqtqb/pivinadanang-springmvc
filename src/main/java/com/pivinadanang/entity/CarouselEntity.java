package com.pivinadanang.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "carousel")
public class CarouselEntity extends BaseEntity{

  @Column (name = "carouselurl")
  private String carouselurl;

  @Column(name = "publicid")
  private String publicId;;


  public CarouselEntity() {
  }

  public CarouselEntity(String carouselurl, String publicId) {
    this.carouselurl = carouselurl;
    this.publicId = publicId;
  }

  public String getCarouselurl() {
    return carouselurl;
  }

  public void setCarouselurl(String carouselurl) {
    this.carouselurl = carouselurl;
  }

  public String getPublicId() {
    return publicId;
  }

  public void setPublicId(String publicId) {
    this.publicId = publicId;
  }
}
