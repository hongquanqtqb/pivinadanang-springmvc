package com.pivinadanang.constant;

public class SystemConstant {
  public static final int ACTIVE_STATUS = 1;
  public static final int INCTIVE_STATUS = 0;

  public static final String INCTIVE_ROLE_USER ="USER";
}
