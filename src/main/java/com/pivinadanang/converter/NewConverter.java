package com.pivinadanang.converter;

import com.pivinadanang.dto.NewDTO;
import com.pivinadanang.entity.NewEntity;
import java.sql.Timestamp;
import org.springframework.stereotype.Component;

@Component
public class NewConverter {
  public NewDTO toDtoList(NewEntity entity){
    NewDTO result = new NewDTO();
    result.setId(entity.getId());
    result.setTitle(entity.getTitle());
    result.setThumbnail(entity.getThumbnail());
    result.setShortDescription(entity.getShortDescription());
    result.setContent(entity.getContent());
    result.setPublicId(entity.getPublicId());
    result.setCreatedBy(entity.getCreatedBy());
    result.setCreatedDate((Timestamp) entity.getCreatedDate());
    result.setCategoryCode(entity.getCategory().getCode());
    return result;
  }
  public NewDTO toDto(NewEntity entity){
    NewDTO result = new NewDTO();
    result.setId(entity.getId());
    result.setTitle(entity.getTitle());
    result.setThumbnail(entity.getThumbnail());
    result.setShortDescription(entity.getShortDescription());
    result.setContent(entity.getContent());
    result.setPublicId(entity.getPublicId());
    result.setCreatedBy(entity.getCreatedBy());
    result.setCategoryCode(entity.getCategory().getCode());
    return result;
  }

  public NewDTO toDto(NewEntity oldeNewEntity, NewDTO newDTO){
    if(newDTO.getTitle()==null){
      newDTO.setTitle(oldeNewEntity.getTitle());
    }
    if(newDTO.getShortDescription()==null){
      newDTO.setShortDescription(oldeNewEntity.getShortDescription());
    }
    if(newDTO.getContent()==null){
      newDTO.setContent(oldeNewEntity.getContent());
    }
    if(newDTO.getCreatedBy()==null){
      newDTO.setCreatedBy(oldeNewEntity.getCreatedBy());
    }
    if(newDTO.getModifiedBy()==null){
      newDTO.setModifiedBy(oldeNewEntity.getModifiedBy());
    }
    if(newDTO.getCreatedBy()==null){
      newDTO.setCreatedDate((Timestamp) oldeNewEntity.getCreatedDate());
    }
    if(newDTO.getPublicId()==null){
      newDTO.setPublicId(oldeNewEntity.getPublicId());
    }
    return newDTO;
  }

  public NewEntity toEntity(NewDTO dto){
    NewEntity result = new NewEntity();
    result.setTitle(dto.getTitle());
    result.setThumbnail(dto.getThumbnail());
    result.setShortDescription(dto.getShortDescription());
    result.setContent(dto.getContent());
    result.setPublicId(dto.getPublicId());
    return result;
  }
  public NewEntity toEntity(NewEntity result, NewDTO dto){
    result.setTitle(dto.getTitle());
    if(dto.getThumbnail()==null){
      result.setThumbnail(result.getThumbnail());
    }else{
      result.setThumbnail(dto.getThumbnail());
    }
    result.setShortDescription(dto.getShortDescription());
    result.setContent(dto.getContent());
    result.setPublicId(dto.getPublicId());
    return result;
  }


}
