package com.pivinadanang.converter;

import com.pivinadanang.dto.CategoryDTO;
import com.pivinadanang.entity.CategoryEntity;
import java.sql.Timestamp;
import org.springframework.stereotype.Component;

@Component
public class CategoryConverter {
  public CategoryDTO toDto(CategoryEntity entity){
    CategoryDTO result = new CategoryDTO();
    result.setId(entity.getId());
    result.setCode(entity.getCode());
    result.setName(entity.getName());
    return result;
  }
    public CategoryDTO toDtoList(CategoryEntity entity){
      CategoryDTO result = new CategoryDTO();
      result.setId(entity.getId());
      result.setCode(entity.getCode());
      result.setName(entity.getName());
      result.setCreatedDate((Timestamp) entity.getCreatedDate());
      result.setCreatedBy(entity.getCreatedBy());
      return result;
    }
  public CategoryEntity toEntity(CategoryDTO dto){
    CategoryEntity result = new CategoryEntity();
    result.setCode(dto.getCode());
    result.setName(dto.getName());
    return result;
  }

  public CategoryEntity toEntity(CategoryEntity categoryEntity, CategoryDTO categoryDTO){
    categoryEntity.setCode(categoryDTO.getCode());
    categoryEntity.setName(categoryDTO.getName());
    return categoryEntity;
  }

}
