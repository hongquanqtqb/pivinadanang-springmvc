package com.pivinadanang.api.web;

import com.pivinadanang.dto.CommentDTO;
import com.pivinadanang.service.ICommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "commentOfWeb")
public class APICommentController {

  @Autowired
  private ICommentService commentService;
  @PostMapping(value = "/api/add-comment")
  public ResponseEntity<CommentDTO> addComment(@RequestBody CommentDTO commentDTO){
    try{
      CommentDTO comment = commentService.save(commentDTO);
      return new ResponseEntity<>(comment, HttpStatus.CREATED);
    }catch (Exception exception){
      exception.printStackTrace();
    }
    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }
}
