package com.pivinadanang.api.admin;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.pivinadanang.dto.NewDTO;
import com.pivinadanang.service.INewService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "newAPIOfAdmin")
public class NewAPI {
  @Autowired
  private INewService newService;

  @Autowired
  private Cloudinary cloudinary;

  @RequestMapping(value = "api/new", method = RequestMethod.DELETE)
  public void delete(@RequestBody long []ids){
    NewDTO newDTO = new NewDTO();
    try{
      newService.delete(ids);
      for(long id: ids){
        newDTO = newService.findById(id);
        try {
          Map r = this.cloudinary.uploader().destroy(newDTO.getPublicId(), ObjectUtils.emptyMap());
        }catch (Exception ex){
          System.out.println("Cannot delete image in cloudinary, Make sure you're connected to internet" + ex.getMessage());
        }
      }
    }catch (Exception e){
      System.out.println("Error is :" + e.getMessage() );
    }
}

}
