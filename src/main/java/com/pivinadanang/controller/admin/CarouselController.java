package com.pivinadanang.controller.admin;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.pivinadanang.dto.CarouselDTO;
import com.pivinadanang.service.ICarouselService;
import java.io.IOException;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller (value = "carousel-admin")
@RequestMapping("/admin/carousel")
public class CarouselController {

  @Autowired
  private ICarouselService carouselService;

  @Autowired
  private Cloudinary cloudinary;

  @GetMapping("list")
  public ModelAndView carouselPage() {
    CarouselDTO carouselDTO = new CarouselDTO();
    ModelAndView modelAndView = new ModelAndView("/admin/carousel/carouselList");
    carouselDTO.setListResult(carouselService.getAll());
    modelAndView.addObject("carousel", carouselDTO);
    return modelAndView;
  }
  @GetMapping("create")
  public ModelAndView createCarouselPage() {
    ModelAndView modelAndView = new ModelAndView("/admin/carousel/carouselCreate");
    return modelAndView;
  }
  @PostMapping("create")
  public String createCarousel(Model model,@ModelAttribute (value = "carousel") CarouselDTO carouselDTO, RedirectAttributes redirectAttributes) {
      if (carouselDTO.getFile().isEmpty()) {
        model.addAttribute("message", "Chưa chọn file");
        model.addAttribute("alert", "danger");
        return "/admin/carousel/carouselCreate";
      } else {
        try {
          Map r = this.cloudinary.uploader().upload(carouselDTO.getFile().getBytes(), ObjectUtils.asMap
              ( "folder", "SpingMVC/Carousel", "resource_type", "auto"));
          String publicId = r.get("public_id").toString();
          String imgUrl = (String) r.get("secure_url");
          carouselDTO.setPublicId(publicId);
          carouselDTO.setCarouselurl(imgUrl);
          carouselService.save(carouselDTO);
          redirectAttributes.addFlashAttribute("message", "Lưu hình ảnh thành công !");
          redirectAttributes.addFlashAttribute("alert", "success");
        } catch (IOException ex) {
          System.out.println("Cannot upload image .Make sure you're connected to internet");
          ex.getMessage();
        }
        return "redirect:/admin/carousel/list";
      }
  }
  @GetMapping("delete/{id}")
  public ModelAndView deleteCarousel(ModelMap model, @PathVariable("id") Long id) {
    CarouselDTO carouselDTO = carouselService.findById(id);
        try {
          try {
            Map r = this.cloudinary.uploader().destroy(carouselDTO.getPublicId(),ObjectUtils.emptyMap());
          }catch (Exception ex){
            System.out.println("Cannot delete image in cloudinary, Make sure you're connected to internet" + ex.getMessage());
          }
          carouselService.deleteById(id);
          model.addAttribute("message", "Xóa ảnh thành công !");
          model.addAttribute("alert", "success");
        }catch (Exception exception){
          model.addAttribute("message", "Server error");
          model.addAttribute("alert", "danger");
          System.out.println("An error occurred" + exception.getMessage());
        }
      return  new ModelAndView("forward:/admin/carousel/list", model);
  }
}