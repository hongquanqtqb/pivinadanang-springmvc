package com.pivinadanang.controller.admin;

import com.pivinadanang.dto.CategoryDTO;
import com.pivinadanang.dto.NewDTO;
import com.pivinadanang.entity.CategoryEntity;
import com.pivinadanang.service.ICategoryService;
import com.pivinadanang.service.INewService;
import com.pivinadanang.util.MessageUtil;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller(value = "categoryControllerOfAdmin")
@RequestMapping ("admin/category")
public class CategoryController {
  @Autowired
  private ICategoryService categoryService;
  @Autowired
  private INewService newService;
  @Autowired
  private MessageUtil messageUtil;
  @Autowired
  private ModelMapper modelMapper;

  @GetMapping("list")
  public String getAllCategory(ModelMap model, @RequestParam (required = false)Map<String,String> param) {
  int page = Integer.parseInt(param.getOrDefault("page", "1"));
  int limit = Integer.parseInt(param.getOrDefault("limit", "10"));
  Pageable pageable = new PageRequest(page-1, limit);
  Page<CategoryEntity> categoryPage = categoryService.getAllCategoryPaginated(pageable);
  Long totalItems = categoryPage.getTotalElements();
  int totalPages = categoryPage.getTotalPages();
  List<CategoryDTO> categoryModel = categoryService.getAllCategoryPaginated(pageable).getContent()
      .stream()
      .map(CategoryEntity -> modelMapper.map(CategoryEntity, CategoryDTO.class))
      .collect(Collectors.toList());;
    model.addAttribute("categoryModel", categoryModel);
    model.addAttribute("totalPage",totalPages);
    model.addAttribute("totalItems",totalItems);
    model.addAttribute("page",page);
  return "/admin/category/categoryList";
  }

  @GetMapping("create")
  public String createCategory(ModelMap model) {
    model.addAttribute("categoryModel",new CategoryDTO());
    return "/admin/category/categoryCreate";
  }

  @PostMapping ("edit")
  public String editCategory(ModelMap model,@Validated @ModelAttribute(value = "categoryModel") CategoryDTO categoryDTO, BindingResult bindingResult) {
    if(bindingResult.hasErrors()) {
      return "/admin/category/categoryCreate" ;
    }else{
        List<CategoryEntity> categoryEntityList = categoryService.findByCode(categoryDTO.getCode().trim());
        if (categoryEntityList.size() > 0) {
          model.addAttribute("message","Thể loại đã tồn tại");
          model.addAttribute("alert","warning");
          return "/admin/category/categoryCreate";
        } else {
            try {
              categoryService.insertOrUpdateCategory(categoryDTO);
              model.addAttribute("message","Thêm thể loại thành công !");
              model.addAttribute("alert","success");
            }catch (Exception exception){
              System.out.println("Có lỗi xẩy ra"+ exception.getMessage());
              exception.printStackTrace();
            }
        }
        return "/admin/category/categoryCreate";
    }
  }
  @GetMapping ("delete/{id}")
  public ModelAndView deleteCategory(ModelMap model,@PathVariable("id") Long id){
    List<NewDTO> newDTOList = newService.findByCategoryId(id);
    if(newDTOList.size() > 0){
      model.addAttribute("message","Cannot delete a parent row: a foreign key constraint fails !");
      model.addAttribute("alert","danger");
      return new ModelAndView("forward:/admin/category/list", model);
    }else{
        try {
          categoryService.delete(id);
          model.addAttribute("message","Xóa thể loại thành công");
          model.addAttribute("alert","success");
        }catch (Exception ex){
          System.out.println("Có lỗi xẩy ra"+ ex.getMessage());
          ex.printStackTrace();
        }
      return new ModelAndView("forward:/admin/category/list", model);
    }
  }
}
