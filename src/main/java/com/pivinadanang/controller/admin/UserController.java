package com.pivinadanang.controller.admin;

import com.pivinadanang.entity.UserEntity;
import com.pivinadanang.service.IUserService;
import java.util.List;
import java.util.Map;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller(value = "userOfAdmin")
@RequestMapping("admin/user")
public class UserController {

  @Autowired
  private IUserService userService;

  @Autowired
  private ModelMapper modelMapper;

  @GetMapping("list")
  public String userList (ModelMap model, @RequestParam (required = false)Map<String,String> param){
    int page = Integer.parseInt(param.getOrDefault("page", "1"));
    int limit = Integer.parseInt(param.getOrDefault("limit", "10"));
    Pageable pageable = new PageRequest(page-1, limit);
    Page<UserEntity> userEntityPage = userService.getAllUserPaginated(pageable);
    List<UserEntity> userEntityList = userEntityPage.getContent();
    model.addAttribute("userModel", userEntityList);
    return "admin/user/list";
  }
}
