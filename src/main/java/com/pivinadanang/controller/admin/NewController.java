package com.pivinadanang.controller.admin;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.pivinadanang.dto.NewDTO;
import com.pivinadanang.service.ICategoryService;
import com.pivinadanang.service.INewService;
import com.pivinadanang.util.MessageUtil;
import java.io.IOException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller(value = "newControllerOfAdmin")
public class NewController {

  @Autowired
  private INewService newService;
  @Autowired
  private ICategoryService categoryService;
  @Autowired
  private MessageUtil messageUtil;

  @Autowired
  private Cloudinary cloudinary;

  @RequestMapping(value = "/admin/new/list", method = RequestMethod.GET)
  public ModelAndView showList(@RequestParam(required = false) Map<String, String> param,
      HttpServletRequest request ) {
    NewDTO model = new NewDTO();
    int page = Integer.parseInt(param.getOrDefault("page", "1"));
    int limit = Integer.parseInt(param.getOrDefault("limit", "10"));
    model.setPage(page);
    model.setLimit(limit);
    ModelAndView mav = new ModelAndView("admin/new/list");
    Pageable pageable = new PageRequest(page - 1, limit);
    model.setListResult(newService.findAll(pageable));
    model.setTotalItem(newService.getTotalItem());
    model.setTotalPage((int) Math.ceil((double) model.getTotalItem() / model.getLimit()));
    if (request.getParameter("message") != null) {
      Map<String, String> message = messageUtil.getMessage(request.getParameter("message"));
      mav.addObject("message", message.get("message"));
      mav.addObject("alert", message.get("alert"));
    }
    mav.addObject("model", model);
    return mav;
  }

  @RequestMapping(value = "/admin/new/create", method = RequestMethod.GET)
  public ModelAndView createNew() {
    ModelAndView mav = new ModelAndView("admin/new/create");
    NewDTO model = new NewDTO();
    mav.addObject("categories", categoryService.findAll());
    mav.addObject("model", model);
    return mav;
  }

  @RequestMapping(value = "/admin/new/update", method = RequestMethod.GET)
  public ModelAndView updateNew(@RequestParam(value = "id", required = false) Long id) {
    ModelAndView mav = new ModelAndView("admin/new/edit");
    NewDTO model = new NewDTO();
    if (id != null) {
      model = newService.findById(id);
    }
    mav.addObject("categories", categoryService.findAll());
    mav.addObject("model", model);
    return mav;
  }

  @RequestMapping(value = "/admin/new/create", method = RequestMethod.POST)
  public String createNew(@Validated @ModelAttribute(value = "model") NewDTO newDTO,
      BindingResult bindingResult, RedirectAttributes redirectAttributes) {
    if (bindingResult.hasErrors()) {
      return "/admin/new/create";
    } else {
      if (newDTO.getFile().isEmpty()) {
        redirectAttributes.addFlashAttribute("message", "Chưa chọn ảnh bài viết");
        redirectAttributes.addFlashAttribute("alert", "danger");
        return "redirect:/admin/new/list";
      } else {
        try {
          Map r = this.cloudinary.uploader().upload(newDTO.getFile().getBytes(), ObjectUtils.asMap
              ("folder", "SpingMVC/Post",
                  "resource_type", "auto"));
          String publicId = r.get("public_id").toString();
          String imgUrl = (String) r.get("secure_url");
          newDTO.setThumbnail(imgUrl);
          newDTO.setPublicId(publicId);
          newService.save(newDTO);
          redirectAttributes.addFlashAttribute("message", "Thêm bài viết thành công");
          redirectAttributes.addFlashAttribute("alert", "success");
          return "redirect:/admin/new/list";
        } catch (IOException ex) {
          System.out.println("Error is: "+ ex.getMessage());
          redirectAttributes.addFlashAttribute("message", "Lỗi server");
          redirectAttributes.addFlashAttribute("alert", "danger");
          return "redirect:/admin/new/list";
        }
      }
    }
  }

  @RequestMapping(value = "/admin/new/update", method = RequestMethod.POST)
  public String updateNew(@Validated @ModelAttribute(value = "model") NewDTO newDTO,
      BindingResult bindingResult, RedirectAttributes redirectAttributes) {
    if (bindingResult.hasErrors()) {
      return "/admin/new/create";
    } else {
      if (newDTO.getFile().isEmpty()) {
        try {
          newService.save(newDTO);
          redirectAttributes.addFlashAttribute("message", "Cập nhật bài viết thành công");
          redirectAttributes.addFlashAttribute("alert", "success");
        } catch (Exception e) {
          e.printStackTrace();
        }
        return "redirect:/admin/new/list";
      } else {
        try {
          NewDTO dto = newService.findById(newDTO.getId());
          Map r = this.cloudinary.uploader().upload(newDTO.getFile().getBytes(), ObjectUtils.asMap
              ("folder", "SpingMVC/Post",
                  "resource_type", "auto"));
          String imgUrl = (String) r.get("secure_url");
          String publicId = r.get("public_id").toString();
          newDTO.setThumbnail(imgUrl);
          newDTO.setPublicId(publicId);
          newService.save(newDTO);
          Map destroy = this.cloudinary.uploader().destroy(dto.getPublicId(), ObjectUtils.emptyMap());
          redirectAttributes.addFlashAttribute("message", "Cập nhật bài viết thành công");
          redirectAttributes.addFlashAttribute("alert", "success");
          return "redirect:/admin/new/list";
        } catch (IOException ex) {
          redirectAttributes.addFlashAttribute("message", "Lỗi server");
          redirectAttributes.addFlashAttribute("alert", "danger");
          return "redirect:/admin/new/list";
        }
      }
    }
  }
}















