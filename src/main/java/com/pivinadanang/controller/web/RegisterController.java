package com.pivinadanang.controller.web;

import com.pivinadanang.dto.UserDTO;
import com.pivinadanang.entity.UserEntity;
import com.pivinadanang.service.IUserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller (value = "register")
public class RegisterController {
  @Autowired
  private IUserService userService;




  @RequestMapping(value = "/register", method = RequestMethod.GET)
  public ModelAndView registerPage() {
    ModelAndView mav = new ModelAndView("login-register/register");
    return mav;
  }
  @RequestMapping(value = "/register", method = RequestMethod.POST)
  public String addUser(Model model,@ModelAttribute (value = "user") UserDTO userDTO) {{
      List<UserEntity> userEntityList = userService.findByUserName(userDTO.getUserName());
      if(userEntityList.size() > 0){
        model.addAttribute("alert", "danger");
        model.addAttribute("message", "Email đã tồn tại !");
        return "login-register/register";
      }else {
        userService.save(userDTO);
        model.addAttribute("alert", "success");
        model.addAttribute("message", "Đăng ký tài khoản thành công !");
        return "login-register/register";
      }
    }

  }
}
