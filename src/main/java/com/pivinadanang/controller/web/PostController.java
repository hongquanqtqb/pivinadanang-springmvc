package com.pivinadanang.controller.web;

import com.pivinadanang.dto.CategoryDTO;
import com.pivinadanang.dto.NewDTO;
import com.pivinadanang.entity.NewEntity;
import com.pivinadanang.service.ICategoryService;
import com.pivinadanang.service.INewService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller(value = "postOfWeb")
public class PostController {
  @Autowired
  private INewService newService;
  @Autowired
  private ICategoryService categoryService;

  @Autowired
  private ModelMapper modelMapper;

  @RequestMapping(value = "/post/list", method = RequestMethod.GET)
  public ModelAndView getAllPost( @RequestParam (required = false) Map<String,String> param){
    NewDTO model = new NewDTO();
    ModelAndView mav = new ModelAndView("web/blog");
    int page = Integer.parseInt(param.getOrDefault("page", "1"));
    int limit = Integer.parseInt(param.getOrDefault("limit","5"));
    model.setPage(page);
    model.setLimit(limit);
    model.setTotalItem(newService.getTotalItem());
    model.setTotalPage((int) Math.ceil((double) model.getTotalItem() / model.getLimit()));
    Pageable pageable = new PageRequest(page - 1, limit);
    String keyword = param.getOrDefault("keyword", "");
    if(keyword.isEmpty()){
      model.setListResult(newService.getPostAll(pageable));
    }else {
      model.setListResult(newService.findByTitleContaining(keyword,pageable));
      mav.addObject("keyword", keyword);
    }
    mav.addObject("model", model);

    List<CategoryDTO> categoryDTOList = categoryService.getAllCategory();
    mav.addObject("categoryList", categoryDTOList);


    Pageable pageRequest = new PageRequest(0, 7);
    List<NewDTO> newRecentPost = newService.findAll(pageRequest);
    mav.addObject("newRecentPost", newRecentPost);

    return mav;
  }

  @RequestMapping(value = "/post/list/postbycategoryid",method = RequestMethod.GET)
  public String getPostByCategoryId(Model model,@RequestParam(required = false) Map<String, String> param ){
    int page = Integer.parseInt(param.getOrDefault("page", "1"));
    int limit = Integer.parseInt(param.getOrDefault("limit","5"));
    Long category_id = Long.parseLong(param.get("id"));
    Pageable pageable = new PageRequest(page-1,limit);
    Page<NewEntity> newEntityPage = newService.findByCategoryId(category_id, pageable);
    Long totalItems = newEntityPage.getTotalElements();
    int totalPages = newEntityPage.getTotalPages();
    List<NewDTO> newDTOList = newEntityPage.getContent()
          .stream()
          .map(NewEntity -> modelMapper.map(NewEntity, NewDTO.class))
          .collect(Collectors.toList());;
    model.addAttribute("model", newDTOList);
    model.addAttribute("totalPages", totalPages);
    model.addAttribute("totalItems", totalItems);
    model.addAttribute("id", category_id);
    model.addAttribute("page", page);


    return "web/postbycategory";
  }
}
