package com.pivinadanang.controller.web;

import com.pivinadanang.dto.CategoryDTO;
import com.pivinadanang.dto.CommentDTO;
import com.pivinadanang.dto.NewDTO;
import com.pivinadanang.service.ICategoryService;
import com.pivinadanang.service.ICommentService;
import com.pivinadanang.service.INewService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;



@Controller(value = "blogSingleOfWeb")
public class BlogSingleController {

  @Autowired
  private INewService newService;

  @Autowired
  private ICategoryService categoryService;

  @Autowired
  private ICommentService commentService;

  @RequestMapping(value = "/blog/blogsingle", method = RequestMethod.GET)
  public ModelAndView singlePage(@RequestParam(value = "id", required = false) Long id) {
    NewDTO newDetail = new NewDTO();
    ModelAndView mav = new ModelAndView("web/blog-single");
    newDetail = newService.findById(id);
    int countNewDetail = commentService.getCountCommentByPostId(id);

    List<CommentDTO> commentDTOList = commentService.getAllCommentByPostId(id);
    mav.addObject("comments", commentDTOList);

    mav.addObject("newDetail", newDetail);
    mav.addObject("countNewDetail",countNewDetail) ;


    List<CategoryDTO> categoryDTOList = categoryService.getAllCategory();
    mav.addObject("categoryList", categoryDTOList);

    Pageable pageableOfRecentPost = new PageRequest(0, 7);
    List<NewDTO> newRecentPost = newService.findAll(pageableOfRecentPost);
    mav.addObject("newRecentPost", newRecentPost);
    return mav;
  }
}
