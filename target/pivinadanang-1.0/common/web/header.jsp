<%@ page import="com.pivinadanang.util.SecurityUtils" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">
        <h1 class="logo me-auto"><a href="<c:url value="/trang-chu"/> "><span>PI VINA</span> DANANG</a></h1>
        <nav id="navbar" class="navbar order-last order-lg-0">
            <ul>
                <li><a href="<c:url value="/trang-chu"/> " class="active">Trang Chủ</a></li>
                <li><a href="<c:url value="/post/list"/>"><span>Blog</span></a>
                </li>
                <li><a href="<c:url value="/contact"/>">Liên Hệ</a></li>
                <li><a href="<c:url value="/dang-nhap"/>">Đăng nhập</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->
        <div class="header-social-links d-flex">
            <a href="https://www.facebook.com/pivinadanang" class="facebook"><i class="bu bi-facebook"></i></a>
        </div>
    </div>
</header><!-- End Header -->

