<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Register</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="stylesheet" href="<c:url value="/template/login/css/bootstrap.min.css"/> " />
</head>
<body style="background-color: rgba(178,174,174,0.33); min-height: 100vh; max-height: fit-content">
    <dec:body/>
</body>
<script src="<c:url value="/template/login/js/bootstrap.min.js"/>"></script>
<script src="<c:url value="/template/login/js/bootstrap.bundle.min.js"/>"></script>
<script src="<c:url value="/template/login/js/popper.js"/>"></script>
</html>