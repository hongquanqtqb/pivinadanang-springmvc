<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title><dec:title default="Trang chủ" /></title>
    <link rel="stylesheet" href="<c:url value='/template/admin/assets/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" href="<c:url value='/template/admin/assets/font-awesome/4.2.0/css/font-awesome.min.css'/>" />
    <link rel="stylesheet" href="<c:url value='/template/admin/assets/css/ace.min.css'/>" class="ace-main-stylesheet" id="main-ace-style" />
    <%--sweetalert--%>
    <link rel="stylesheet" href="<c:url value='/template/admin/sweetalert/sweetalert2.min.css'/>"/>
    <script src="<c:url value='/template/admin/sweetalert/sweetalert2.min.js'/>"></script>


    <link rel="stylesheet" href="<c:url value='/template/admin/assets/fonts/fonts.googleapis.com.css'/>" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <script type='text/javascript' src='<c:url value="/template/admin/assets/js/jquery-2.2.3.min.js" />'></script>
    <script src="<c:url value='/ckeditor/ckeditor.js'/>"></script>
    <script src="<c:url value='/ckfinder/ckfinder.js'/>"></script>
    <script src="<c:url value='/ckeditor/skins/kama/skin.js'/>"></script>


</head>
<body class="no-skin">
	<!-- header -->
    <%@ include file="/common/admin/header.jsp" %>
    <!-- header -->
	<div class="main-container" id="main-container">
        <script type="text/javascript">
          try{ace.settings.check('main-container' , 'fixed')}catch(e){}
        </script>
		<!-- header -->
    	<%@ include file="/common/admin/menu.jsp" %>
    	<!-- header -->
		<dec:body/>
		<!-- footer -->
    	<%@ include file="/common/admin/footer.jsp" %>
    	<!-- footer -->
    	<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse display">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
		</a>
	</div>
    <script src="<c:url value='/template/admin/paging/jquery.twbsPagination.js'/>"></script>
    <script src="<c:url value='/template/admin/assets/js/bootstrap.min.js'/>"></script>
    <script src="<c:url value='/template/admin/assets/js/ace-elements.min.js'/>"></script>
    <script src="<c:url value='/template/admin/assets/js/ace.min.js'/>"></script>
    <script src="<c:url value='/template/admin/assets/js/jquery-ui.min.js'/>"></script>
</body>
</html>