
function addComment(postId) {
  fetch("/api/add-comment", {
    method: 'POST',
    body: JSON.stringify({
      "content": document.getElementById("commentId").value,
      "postId": postId
    }),
    headers: {
      "Content-Type": "application/json"
    }
  }).then(function (res) {
    return res.json()
  }).then(function (posts) {
    console.info(posts);
    let area = document.getElementById("commentArea");
    area.innerHTML = `
                          <div class="d-flex">
                            <div class="comment-img">
                             <img src="https://res.cloudinary.com/dzmfnjwxq/image/upload/v1658818473/SpingMVC/usericon_qhs4yk.png"/>
                            </div>
                            <div class="comment-content">
                              <h5><a href="">${posts.createdBy}</a></h5>
                              <time>${moment(posts.createdDate).fromNow()}</time>
                              <p>${posts.content}</p>
                            </div>
                          </div>
                        `
        + area.innerHTML
  }).catch(function (err) {
    console.log("Có lỗi xẩy ra")
  })
}