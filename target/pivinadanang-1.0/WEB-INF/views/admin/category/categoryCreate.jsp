<%@include file="/common/taglib.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<c:url var="categoryList" value="/admin/category/list"/>
<html>
<head>
    <meta charset="UTF-8">
    <title>Thêm thể loại</title>
</head>
<body>
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Thêm thể loại bài viết</a></li>
            </ul>
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <c:if test="${not empty message}">
                        <div class="alert alert-${alert}">
                                ${message}
                        </div>
                    </c:if>
                    <form:form class="form-horizontal" role="form" modelAttribute="categoryModel" action="/admin/category/edit" method="post">
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="code">Code</label>
                            <div class="col-md-6">
                                <form:errors path="code" cssClass="error" cssStyle="color: red"/>
                                <form:input path="code" class="form-control" name="code" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="name">Name</label>
                            <div class="col-md-6">
                                <form:errors path="name" cssClass="error" cssStyle="color: red"/>
                                <form:input path="name" class="form-control" name="name"/>
                            </div>
                        </div>
                        <div class="clearfix form-actions">
                            <div class="col-md-offset-4 col-md-8">
                                    <button class="btn btn-info" type="submit">
                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                        Thêm thể loại
                                    </button>
                                <button class="btn" type="reset">
                                    <a href="${categoryList}?page=1&limit=10">
                                        <i class="ace-icon fa fa-undo bigger-110"></i>
                                        Hủy
                                    </a>
                                </button>
                            </div>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

</script>
</body>
</html>