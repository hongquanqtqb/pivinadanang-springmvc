<%@include file="/common/taglib.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Chỉnh sửa bài viết</title>
</head>
<body>
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Chỉnh sửa bài viết</a></li>
            </ul>
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <form:form role="form" modelAttribute="model" action="/admin/new/update" method="post" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="categoryCode">Chọn Thể Loại</label>
                            </div>
                            <div class="col-lg-9">
                                <form:select path="categoryCode" id="categoryCode">
                                    <form:option value="" lasbel="---Chọn thể loại---"/>
                                    <form:options items="${categories}" />
                                </form:select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label  for="title"> Tiêu đề </label>
                            </div>
                            <div class="col-lg-9">
                                <form:input path="title" id="title" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-2">
                                <label for="file">  Ảnh đại diện </label>
                                <input class="form-control-file" type="file" name="file" id="file" accept="image/*" onchange="chooseFile(this)"/>
                            </div>
                            <div class="col-lg-3">
                                <img src="${model.thumbnail}"id="image" width="250px" height="180"/>
                            </div>
                            <div class="col-lg-1">
                                <label for="shortDescription">Mô tả ngắn:</label>
                            </div>
                            <div class="col-lg-5">
                                <form:textarea path="shortDescription"  id="shortDescription" rows="8" cols="3" cssClass="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                           <div class="col-lg-2">
                               <label for="content">Nội Dung:</label>
                           </div>
                            <div class="col-lg-9">
                                <form:textarea path="content" id="content"/>
                            </div>
                        </div>
                        <form:hidden path="id" id="newid"/>
                        <div class="clearfix form-actions">
                            <div class="col-md-offset-5 col-md-7">
                                    <button class="btn btn-info" type="submit" id="updateNew">
                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                        Cập nhật bài viết
                                    </button>
                                <button class="btn" type="reset">
                                    <a href="<c:url value='/admin/new/list?page=1&limit=10'/>">
                                        <i class="ace-icon fa fa-undo bigger-110"></i>
                                        Hủy
                                    </a>
                                </button>
                            </div>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
  var editor = '';
  $(document).ready(function () {
    editor = CKEDITOR.replace('content');
    CKFinder.setupCKEditor(editor,'${pageContext.request.contextPath}/ckfinder');
  });

  function chooseFile(fileInput){
    if(fileInput.files && fileInput.files[0]){
      var reader = new FileReader();
      reader.onload = function (e){
        $('#image').attr('src',e.target.result);
      }
      reader.readAsDataURL(fileInput.files[0]);
    }
  }
</script>
</body>
</html>