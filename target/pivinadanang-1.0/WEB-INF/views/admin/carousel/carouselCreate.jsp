<%@include file="/common/taglib.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<c:url var="categoryList" value="/admin/category/list"/>
<html>
<head>
    <meta charset="UTF-8">
    <title>Thêm Silde</title>
</head>
<body>
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li><i class="ace-icon fa fa-home home-icon"></i><a href="#">Thêm Slide</a></li>
            </ul>
        </div>
        <div class="page-content">
            <div class="row" style="margin-top: 30px">
                <div class="col-xs-12">
                    <div class="row">
                        <c:if test="${not empty message}">
                            <div class="alert alert-${alert}">
                                    ${message}
                            </div>
                        </c:if>
                    </div>
                    <form:form class="form-horizontal" modelAttribute="carousel" action="/admin/carousel/create" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type ="file" name="file" id="file"  class="form-control-file" onchange="chooseFile(this)"/>
                        </div>
                        <div class="form-group">
                            <img src="..." class="img-fluid " alt="Responsive image" id="image">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Lưu Hình Ảnh</button>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
  function chooseFile(fileInput){
    if(fileInput.files && fileInput.files[0]){
      var reader = new FileReader();
      reader.onload = function (e){
        $('#image').attr('src',e.target.result);
      }
      reader.readAsDataURL(fileInput.files[0]);
    }
  }
</script>
</body>
</html>