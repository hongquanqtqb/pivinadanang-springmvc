<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>User Managerment </title>
</head>
<body>
<div class="main-content">
    <form>
        <div class="main-content-inner">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li><i class="ace-icon fa fa-home home-icon"></i><a href="#">LIST OF USER</a></li>
                </ul>
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <c:if test="${not empty message}">
                            <div class="alert alert-${alert}">
                                    ${message}
                            </div>
                        </c:if>
                        <div class="widget-box table-filter">
                            <div class="table-btn-controls">
                                <div class="pull-right tableTools-container">
                                    <div class="dt-buttons btn-overlap btn-group">
                                        <c:url var="createUser" value=""/>
                                        <a flag="info"
                                           class="dt-button buttons-colvis btn btn-white btn-primary btn-bold"
                                           data-toggle="tooltip" title="Thêm thể loại" href="${createUser}">
                                            <span><i class="fa fa-plus-circle bigger-110 purple"></i></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>FullName</th>
                                            <th>UserName</th>
                                            <th>Status</th>
                                            <th>Role</th>
                                            <th>Thao tác</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:forEach var="item" items="${userModel}">
                                            <tr>
                                                <td>${item.id}</td>
                                                <td>${item.fullName}</td>
                                                <td>${item.userName}</td>
                                                <td>${item.status}</td>
                                                <td> <a href="<c:url value="/admin/category/delete/${item.id}"/>"
                                                        class="dt-button buttons-html5 btn btn-white btn-danger btn-bold"
                                                        title="xóa thể loại"
                                                        id="deleteButton">
                                                    <span><i class="fa fa-trash-o bigger-110 pink"></i></span>
                                                </a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>

                                    <div class="modal" tabindex="-1" role="dialog" id="deletModal">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Confirm delete</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure you want to delete this record ?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="" type="button" class="btn btn-danger" id="delRef">Yes, Delete</a>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <ul class="pagination" id="pagination"></ul>
                                    <input type="hidden" value="" id="page" name="page"/>
                                    <input type="hidden" value="" id="limit" name="limit"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>

  $(document).ready(function () {
    $('table #deleteButton').on('click',function (event){
      event.preventDefault();
      var href = $(this).attr('href');
      $('#deletModal #delRef').attr('href',href);
      $('#deletModal').modal();
    });
  });
</script>
</body>

</html>