<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Trang chủ</title>
</head>
<body>
    <div class="container">
        <div id="blog" class="blog">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                        <c:forEach var="item" items="${model}">
                            <div class="row gy-4 posts-list">
                                    <div class="col-lg-4">
                                            <div class="img-thumbnail">
                                                <img src="${item.thumbnail}" class="img-fluid" style="width:240px;height:140px">
                                            </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <article class="d-flex flex-column">
                                            <h2 class="title">
                                                <c:url var="newDetail" value="/blog/blogsingle">
                                                    <c:param name="id" value="${item.id}"/>
                                                </c:url>
                                                <a href="${newDetail}">${item.title}</a>
                                            </h2>
                                            <div class="meta-top">
                                                <ul>
                                                    <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a href="blog-details.html">${item.createdBy}</a></li>
                                                    <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a href="blog-details.html"><fmt:formatDate pattern="dd-MM-yyyy" value="${item.createdDate}"/></a></li>
                                                </ul>
                                            </div>
                                            <div class="content">
                                                <p>${item.shortDescription}</p>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                        </c:forEach>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            TotalItems: ${totalItems} - Page ${page} of ${totalPages}
        </div>
        <form action="<c:url value="/post/list/postbycategoryid"/> " method="get" id="formSubmit">
            <nav aria-label="...">
                <input hidden="text" value="${id}" id="id" name="id"/>
                <input hidden="text" value="" id="page" name="page"/>
                <input hidden="text" value="" id="limit" name="limit"/>
                <ul class="pagination justify-content-center" id="pagination-post" style="margin-top: 20px"></ul>
            </nav>
        </form>
    </div>
<script>
  var currentPage = ${page};
  var totalPage = ${totalPages};
  $(function () {
    window.pagObj = $('#pagination-post').twbsPagination ({
      totalPages: totalPage,
      visiblePages: 5,
      startPage: currentPage,
      onPageClick: function (event, page) {
        if (currentPage != page) {
          $('#limit').val(5);
          $('#page').val(page);
          $('#formSubmit').submit();
        }
      }
    });
  });
</script>
</body>
</html>