<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Trang chủ</title>
</head>
<body>
<section id="breadcrumbs" class="breadcrumbs">
    <div class="container">
        <div class="d-flex justify-content-between align-items-center">
            <h2>Thông Tin Liên Hệ </h2>
            <ol>
                <li><a href="<c:url value="/trang-chu"/>">Trang Chủ</a></li>
                <li>Liên Hệ</li>
            </ol>
        </div>
    </div>
</section><!-- End Breadcrumbs -->

<div class="map-section">
    <iframe style="border:0; width: 100%; height: 350px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3833.668909634578!2d108.13958831417004!3d16.08266194335129!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31421f3266e1a309%3A0xe82acc70534b4bc2!2sC%C3%94NG%20TY%20TNHH%20PI%20VINA%20DANANG!5e0!3m2!1svi!2s!4v1634870489553!5m2!1svi!2s" frameborder="0" allowfullscreen></iframe>
</div>

<div class="container">
    <div id="contact" class="contact">
        <div class="row justify-content-center" data-aos="fade-up">
            <div class="col-lg-10">
                <div class="info-wrap">
                    <div class="row">
                        <div class="col-lg-4 info">
                            <i class="bi bi-geo-alt"></i>
                            <h4> Địa Chỉ:</h4>
                            <p>Lô P2 Đường số 6 - KCN Hòa Khánh, Liên Chiểu, TP Đà Nẵng</p>
                        </div>
                        <div class="col-lg-4 info mt-4 mt-lg-0">
                            <i class="bi bi-envelope"></i>
                            <h4>Email:</h4>
                            <p>quan@poongin.co.kr<br>pidn-hr@poongin.co.kr</p>
                        </div>
                        <div class="col-lg-4 info mt-4 mt-lg-0">
                            <i class="bi bi-phone"></i>
                            <h4>Điện Thoại</h4>
                            <p>0236-3796-756(133)<br>0236-3796-755(130)</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5 justify-content-center" data-aos="fade-up">
            <div class="col-lg-10">
                <form>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" required>
                        </div>
                        <div class="col-md-6 form-group mt-3 mt-md-0">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" required>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
                    </div>
                    <div class="form-group mt-3">
                        <textarea class="form-control" name="message" rows="5" placeholder="Message" required></textarea>
                    </div>
                    <div class="my-3 text-center">
                        <button type="submit" class="btn btn-lg btn-primary">Send Message</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>