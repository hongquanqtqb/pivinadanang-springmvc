<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="/common/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Trang chủ</title>
</head>
<body>
<div class="container">
    <div id="blog" class="blog">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="row gy-4 post-single">
                    <div class="post-image">
                        <img src="${newDetail.thumbnail}" class="img-fluid" alt="Responsive image">
                    </div>
                    <div class="title">
                        <h2><a id="title" href="">${newDetail.title}</a></h2>
                    </div>
                    <div class="meta-top">
                        <ul>
                            <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a
                                    href="blog-single.html">${newDetail.createdBy}</a></li>
                            <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a
                                    href="blog-single.html"><fmt:formatDate pattern="dd-MM-yyyy"
                                                                            value="${newDetail.createdDate}"/></a>
                            </li>
                            <li class="d-flex align-items-center"><i class="bi bi-chat-dots"></i> <a
                                    href="blog-single.html">12 Comments</a></li>
                        </ul>
                    </div>
                    <div class="content">
                        <p>${newDetail.content}</p>
                    </div>
                    <div class="footer">
                        <i class="bi bi-folder"></i>
                        <ul class="cats">
                            <li><a href="#">Business</a></li>
                        </ul>
                        <i class="bi bi-tags"></i>
                        <ul class="tags">
                            <li><a href="#">Creative</a></li>
                            <li><a href="#">Tips</a></li>
                            <li><a href="#">Marketing</a></li>
                        </ul>
                    </div>
                </div>
                <div class="blog-comments">
                    <div class="reply-form">
                        <h4>Nhập bình luận của bạn</h4>
                            <div class="row">
                                <div class="col form-group">
                                    <textarea name="comment" id="commentId" class="form-control" placeholder="Your Comment*" required></textarea>
                                </div>
                            </div>
                            <button type="button" onclick="addComment(${newDetail.id})" class="btn btn-primary">Post Comment</button>
                    </div>
                    <h4 class="comments-count">${countNewDetail} Comment</h4>
                    <div id="commentArea" class="comment">
                        <c:forEach items="${comments}" var="comments">
                            <div class="d-flex">
                                <div class="comment-img">
                                    <img class="img-circle" src="https://res.cloudinary.com/dzmfnjwxq/image/upload/v1658818473/SpingMVC/usericon_qhs4yk.png"/>
                                </div>
                                <div class="comment-content">
                                    <h5><a href="">${comments.createdBy}</a></h5>
                                    <time>${comments.createdDate}</time>
                                    <p>${comments.content}</p>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="sidebar">
                    <h3 class="sidebar-title">Search</h3>
                    <form action="<c:url value='/post/list'/>" method="get" id="formSubmit">
                        <div class="sidebar-item search-form input-group"
                             style="background: #fff;border: 1px solid #ddd;padding: 1px 1px; position: relative;">
                            <input type="text" class="form-control" name="keyword" id="keyword"
                                   placeholder="Nhập từ khoa tìm kiếm">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-outline-secondary"
                                        style="background: #5ca3ea;color: #fff;"><i
                                        class="bi bi-search"></i></button>
                                <button type="button" id="btnClear"
                                        class="btn btn-outline-secondary"
                                        style="background: #5ca3ea; color: #fff;"><i
                                        class="bi bi-arrow-clockwise"></i></button>
                            </div>
                        </div>
                    </form>
                    <div class="sidebar-item categories">
                        <h3 class="sidebar-title">Categories</h3>
                        <c:forEach var="category" items="${categoryList}">
                            <c:url var="postListById" value="/post/list/postbycategoryid">
                                <c:param name="id" value="${category.id}"/>
                            </c:url>
                            <ul class="mt-3">
                                <li><a href="${postListById}">${category.name} </a></li>
                            </ul>
                        </c:forEach>
                    </div>
                    <div class="sidebar-item recent-posts">
                        <h3 class="sidebar-title">Recent Posts</h3>
                        <div class="mt-3">
                            <div class="post-item mt-3">
                                <c:forEach var="newRecentPost" items="${newRecentPost}">
                                    <img src="${newRecentPost.thumbnail}" alt=""
                                         class="flex-shrink-0">
                                    <div>
                                        <c:url var="postDetail" value="/blog/blogsingle">
                                            <c:param name="id" value="${newRecentPost.id}"/>
                                        </c:url>
                                        <h4><a href="${postDetail}">${newRecentPost.title}</a></h4>
                                        <time><fmt:formatDate pattern="dd-MM-yyyy"
                                                              value="${newRecentPost.createdDate}"/></time>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
  $(document).ready(function(){
    let dates = document.querySelectorAll(".comment-content > time")
    for (let i = 0; i < dates.length; i++) {
      let d = dates[i]
      d.innerText = moment(d.innerText).fromNow()
    }
  });
  var input = document.getElementById("keyword");
  input.addEventListener("keypress", function (event) {
    if (event.key === "Enter") {
      event.preventDefault();
      document.getElementById("btnSearch").click();
    }
  });
  const btn = document.getElementById('btnClear');
  btn.addEventListener('click', function handleClick(event) {
    event.preventDefault();
    const keywordInput = document.getElementById('keyword');
    console.log(keywordInput.value);
    keywordInput.value = '';
  });
</script>
</body>
</html>